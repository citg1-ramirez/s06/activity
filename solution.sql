-- Charmaine Mae I. Ramirez BSIT-3-G1


--Q2
a. List the books Authored by Marjorie Green.
	Author ID 			Author Name
		213-46-8915			Marjorie Green 

	Book ID 			Book Title
		BU1032				The Busy Executive's Database Guide
		BU2075				You Can Combat Computer Stress!
b. List the books Authored by Michael O'Leary.
	Author ID 			Author Name
		267-41-2394			Michael OLeary

	Book ID 			Book Title
		BU1111				Cooking with Computers
		TC7777	

c. Write the author/s of "The Busy Executives Database Guide".
	Book ID 			Book Title
		BU1032				The Busy Executives Database Guide
	Author ID 			Author Name		
		213-46-8915			Marjorie Green

d. Identify the publisher of "But Is It User Friendly?".
	Book ID 			Book Title
		PC1035				But Is It User Friendly?
	Publisher ID 		Publisher Name		
		1389				Algodata Infosystems

e. List the books published by Algodata Infosystems.
	Publisher ID 		Publisher Name		
		1389				Algodata Infosystems

	Book ID 			Book Title
		BU1032				The Busy Executives Database Guide
		BU1111				Cooking with Computers
		BU7832				Straight Talk About Computers
		PC1035				But Is It User Friendly?
		PC8888				Secrets of Silicon Valley
		PC9999				Net Etiquette




--Q3 Steps

-- 1. Initailize the mysql in the terminal
mysql -u root -p


-- 2. Create a new database.
CREATE DATABASE blog_db;

--3. Verify if the database was successfully created.
SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| dbartist           |
| information_schema |
| music_store        |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+

--4. Selecting the database 'blog_db' as the database I want to use.
USE blog_db; 


-- 5. Create 'users' table.
CREATE TABLE users (
id INT AUTO_INCREMENT NOT NULL, 
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY (id)
);

-- 6. Verify if the 'users' table was created.
SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| users             |
+-------------------+

DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(300) | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+


-- 7. Create 'posts' table.
CREATE TABLE posts (
id INT AUTO_INCREMENT NOT NULL, 
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_posts_user_id
FOREIGN KEY (author_id) REFERENCES users(id)
ON UPDATE cascade
ON DELETE restrict
);


-- 8. Verify if the 'posts' table was created.
SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
| users             |
+-------------------+

DESCRIBE POSTS;
+------------------+---------------+------+-----+---------+----------------+
| Field            | Type          | Null | Key | Default | Extra          |
+------------------+---------------+------+-----+---------+----------------+
| id               | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id        | int(11)       | NO   | MUL | NULL    |                |
| title            | varchar(500)  | NO   |     | NULL    |                |
| content          | varchar(5000) | NO   |     | NULL    |                |
| datetime_posted  | datetime      | NO   |     | NULL    |                |
+------------------+---------------+------+-----+---------+----------------+


-- 9. Create 'posts_comments' table.
CREATE TABLE posts_comments(
id INT AUTO_INCREMENT NOT NULL,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000),
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_comments_posts
FOREIGN KEY (post_id) REFERENCES posts(id)
ON UPDATE cascade
ON DELETE restrict,
CONSTRAINT fk_posts_comments_users
FOREIGN KEY(user_id) REFERENCES users(id)
ON UPDATE cascade
ON DELETE restrict
);

-- 10. Verify if the 'posts_comments' table was created.
SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
| post_comments    |
| users             |
+-------------------+

DESCRIBE posts_comments;
+--------------------+---------------+------+-----+---------+----------------+
| Field              | Type          | Null | Key | Default | Extra			 |
+--------------------+---------------+------+-----+---------+----------------+
| id                 | int(11)       | NO   | PRI | NULL    | auto_increment |
| post_id            | int(11)       | NO   | MUL | NULL    |				 |
| user_id            | int(11)       | NO   | MUL | NULL    |				 |
| content            | varchar(5000) | YES  |     | NULL    |				 |
| datetime_commented | datetime      | NO   |     | NULL    |				 |
+--------------------+---------------+------+-----+---------+----------------+


-- 11. Create 'posts_likes' table.
CREATE TABLE posts_likes(
id INT AUTO_INCREMENT NOT NULL,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_like DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_posts_likes_posts
FOREIGN KEY (post_id) REFERENCES posts(id)
ON UPDATE cascade
ON DELETE restrict,
CONSTRAINT fk_posts_likes_users
FOREIGN KEY (user_id) REFERENCES users(id)
ON UPDATE cascade
ON DELETE restrict
);


-- 12. Verify if the 'posts_likes' table was created.
SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
| post_comments    |
| post_likes       |
| users             |
+-------------------+

DESCRIBE post_likes;
+---------------+----------+------+-----+---------+----------------+
| Field         | Type     | Null | Key | Default | Extra          |
+---------------+----------+------+-----+---------+----------------+
| id            | int(11)  | NO   | PRI | NULL    | auto_increment |
| post_id       | int(11)  | NO   | MUL | NULL    |                |
| user_id       | int(11)  | NO   | MUL | NULL    |                |
| datetime_like | datetime | NO   |     | NULL    |                |
+---------------+----------+------+-----+---------+----------------+